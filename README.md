Relation Extractor :
I built a relation extractor to identify institution relation instances from Wikipedia sentences. The relation institution(x, y) indicates that a person x studied in the institution y.
Entity Pair Sentence Label
(Bill Gates, Harvard) Bill Gates enrolled in Harvard. Yes
(Larry Page, Stanford) Larry Page was enrolled at Stanford Yes
For classification, I used LibSVM with linear kernel and C= 0.1 for most of the models as it gave better results.
I implemented following models for relation extraction:
1. Manual Regex
In this model, I manually found the patterns occurring more often in positive class and added 6 patterns to filter the valid sentences using regular expression matching. Following are the patterns which provided me better coverage:
- '(.*?) graduat.*' : to cover sentences with pattern as graduated from, graduated in, graduate, graduating, etc.
- ' (.*?) attend.*' : pattern covered is: attended <University>, attended in, etc.
- '(.*?) stud.* (at|from|in|of)' : pattern covered is: studied in year, student of, studies in, etc.
- '(.*?) received a .* (from)?' : pattern : received a degree, received a diploma from, etc
- '(.*?) enroll.* ' : pattern : enrolled in, enrolling in, etc.
To increase the coverage, I also added POS tag features which checks for pattern “matriculated at NNP” which means it checks if ‘matriculated at’ is followed by a noun phrase, which usually is the name of the institution.
I observed that adding this feature increased the F1 measure compared to without POS tag version.
2. BOW
In this model, I am taking all the words which occur between the first occurrence of name and of institution. Then I generated features by using the bag of words model, where each feature represents a word in intermediate text. For this model, I used split method to get token words.
3. BOW with tokenizer
This model is like BOW model. The only difference is how the tokens are split. In BOW, split is used, whereas in this model, I used nltk tokenizer, word_tokenize. It generated better tokens by ignoring the punctuations from middle of the words like “graduated,” to “graduated”
4. Brown Clusters
In BOW, words were directly used as features. This resulted in sparse features and large number of features with less information. To overcome this, I used brown clusters to cluster similar words and use these clusters as features. In Brown clusters, I observed that if clusters are less, then the classifier predicts only one class. To avoid this I increased the number of clusters used upto 100 (due to machine limitation). Also, instead of using entire cluster id as feature, I used prefix of size upto 10 which gave better results. With clusters c = 50, in brown clusters, I was getting precision, recall and f1 for no class as zero. For yes class, I got precision = 0.654, recall = 1.00 and f1 = 0.791 with c 50 in brown clusters and it was same for all prefix lengths. I got better results for full length cluster id than with prefix.
For prefix length: 10
Precision: 0.681
Recall: 0.863
F1-measure: 0.765
For prefix length: full length
Precision: 0.654
Recall: 1.00
F1-measure: 0.791
5. Dependency
In this model I built syntactic features using the syntactic dependency tree. I used StanfordDependencyParser to generate the dependency trees. Then I wrote features on the triples that I got, where each triple had a (left child, its tag), the relation, (right child, its tag) as a tuple. The three features are as follows:
- tag == "nsubj" and left_tag == "VBD" and (right_tag == "PRP" or right_tag == "NNP") : to check syntax where for verb such as graduated, the subject is either pronoun(he/she) or noun(name).
- tag == "nsubjpass" and left_tag == "VBN" and right_tag == "PRP": to check syntax where passive nominal subject relation is observed between past participle of a verb and a personal pronoun.
- tag == "advmod" and left_tag == "VBD" and right_tag == "IN" : to check syntax where past tense verb was adverbial modifier for preposition.
6. Kitchen Sink
In this model, I combined all the above features and then generated features.
