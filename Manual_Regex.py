import re
from nltk import word_tokenize, pos_tag

TEST_DATA_PATH = "test.tsv"
TRAIN_DATA_PATH = "train.tsv"

def matriculated_at(tagged):
    return any(['matriculated', 'at', 'NNP'] == [tagged[i][0], tagged[i+1][0], tagged[i+2][1]] for i in range(len(tagged) - 2))

def manual_regex(data):
    true_positives = 0
    pred_positives = 0
    all_positives = 0
    
    for fp in [data]:
        with open(fp, encoding="utf8") as f:
            for line in f:
                institution, person, snippet, intermediate_text, judgment = line.split("\t")
                judgment = judgment.strip()
                snippet_splitted = snippet.split()
                tagged = pos_tag(snippet_splitted)
				
                #graduated|graduating
                #attending|attended
                #studied|student|studying
                #received a .* from
                #enrolled
                matched = re.search(r'(.*?) graduat.* ', snippet, re.M | re.I) or \
                    re.search(r'(.*?) attend.*', snippet, re.M | re.I) or \
                    re.search(r'(.*?) stud.* (at|from|in|of)', snippet, re.M | re.I) or \
                    re.search(r'(.*?) received a .* (from)?', snippet, re.M | re.I) or \
                    re.search(r'(.*?) enroll.*', snippet, re.M | re.I) or \
                    matriculated_at(tagged)
					
                if matched:
                    pred_positives += 1					
                if judgment == "yes":
                    all_positives += 1
                if matched and judgment == "yes":
                    true_positives += 1				
    		
    precision = true_positives/pred_positives
    recall = true_positives / all_positives
    f1 = 2*((precision*recall)/(precision+recall))
    print("Precision: ", precision)
    print("Recall: ", recall)
    print("F-Measure: ", f1)

if __name__ == "__main__":
    print("On Train data")
    manual_regex(TRAIN_DATA_PATH)
    print("On Test data")
    manual_regex(TEST_DATA_PATH)
