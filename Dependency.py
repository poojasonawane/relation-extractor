from nltk.tokenize import word_tokenize
from nltk.parse.stanford import StanfordDependencyParser

PATH_TO_JAR = 'stanford-parser-full-2016-10-31/stanford-parser-full-2016-10-31/stanford-parser.jar'
PATH_TO_MODELS_JAR = 'stanford-parser-full-2016-10-31/stanford-parser-full-2016-10-31/stanford-parser-3.7.0-models.jar'
    
TEST_DATA_PATH = "test.tsv"
TRAIN_DATA_PATH = "train.tsv"

def parse_data(train_data, test_data):
    """
    Input: path to the data file
    Output: (1) a list of tuples, one for each instance of the data, and
            (2) a list of all unique tokens in the data

    Parses the data file to extract all instances of the data as tuples of the form:
    (person, institution, judgment, full snippet, intermediate text)
    where the intermediate text is all tokens that occur between the first occurrence of
    the person and the first occurrence of the institution.

    Also extracts a list of all tokens that appear in the intermediate text for the
    purpose of creating feature vectors.
    """
    all_tokens = []
    data = []
    for fp in [train_data, test_data]:
        with open(fp, encoding="utf8") as f:
            for line in f:
                institution, person, snippet, intermediate_text, judgment = line.split("\t")
                judgment = judgment.strip()

                #tokens = intermediate_text.split()
                tokens = word_tokenize(intermediate_text)
                for t in tokens:
                    t = t.lower()
                    if t not in all_tokens:
                        all_tokens.append(t)
                data.append((person, institution, judgment, snippet, intermediate_text))
    return data, all_tokens


def create_feature_vectors(data, all_tokens):
    """
    Input: (1) The parsed data from parse_data()
             (2) a list of all unique tokens found in the intermediate text
    Output: A list of lists representing the feature vectors for each data instance

    Creates feature vectors from the parsed data file. These features include
    bag of words features representing the number of occurrences of each
    token in the intermediate text (text that comes between the first occurrence
    of the person and the first occurrence of the institution).
    This is also where any additional user-defined features can be added.
    """
    feature_vectors = []
    for instance in data:
        # dep parser features
        # Gets the number of occurrences of each token
        # in the intermediate text
        dependency_parser = StanfordDependencyParser(path_to_jar=PATH_TO_JAR, path_to_models_jar=PATH_TO_MODELS_JAR)		
        feature_vector = [0, 0, 0]
        intermediate_text = instance[4]
        intermediate_text = intermediate_text[:150]
        parse = dependency_parser.raw_parse(intermediate_text)
        dep = parse.__next__()
        
        for triple in list(dep.triples()):
            left = triple[0][0]
            left_tag = triple[0][1]
            tag = triple[1]
            right = triple[2][0]
            right_tag = triple[2][1]
            if tag == "nsubj" and left_tag == "VBD" and (right_tag == "PRP" or right_tag == "NNP"):
                feature_vector[0] = 1	
            if tag == "nsubjpass" and left_tag == "VBN" and right_tag == "PRP":
                feature_vector[1] = 1
            if tag == "advmod" and left_tag == "VBD" and right_tag == "IN":
                feature_vector[2] = 1			
        
        # Class label
        judgment = instance[2]
        feature_vector.append(judgment)

        feature_vectors.append(feature_vector)
    return feature_vectors


def generate_arff_file(feature_vectors, all_tokens, out_path):
    """
    Input: (1) A list of all feature vectors for the data
             (2) A list of all unique tokens that occurred in the intermediate text
             (3) The name and path of the ARFF file to be output
    Output: an ARFF file output to the location specified in out_path

    Converts a list of feature vectors to an ARFF file for use with Weka.
    """
    with open(out_path, 'w') as f:
        # Header info
        f.write("@RELATION institutions\n")
        for i in range(3):
            f.write("@ATTRIBUTE syntactic_{} INTEGER\n".format(i))
        
        # Classes
        f.write("@ATTRIBUTE class {yes,no}\n")

        # Data instances
        f.write("\n@DATA\n")
        for fv in feature_vectors:
            features = []
            for i in range(len(fv)):
                value = fv[i]
                if value != 0:
                    features.append("{} {}".format(i, value))
            entry = ",".join(features)
            f.write("{" + entry + "}\n")

if __name__ == "__main__":
    data, all_tokens = parse_data(TRAIN_DATA_PATH, TEST_DATA_PATH)
    feature_vectors = create_feature_vectors(data, all_tokens)
    generate_arff_file(feature_vectors[:6000], all_tokens, "train_dependency.arff")
    generate_arff_file(feature_vectors[6000:], all_tokens, "test_dependency.arff")
    