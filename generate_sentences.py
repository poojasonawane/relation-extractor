TEST_DATA_PATH = "test.tsv"
TRAIN_DATA_PATH = "train.tsv"

def cluster_train_data(train_data, test_data):
    with open("sentences.txt", 'w', encoding="utf8") as write_file:
        for fp in [train_data, test_data]:
            with open(fp, encoding="utf8") as f:
                for line in f:
                    institution, person, snippet, intermediate_text, judgment = line.split("\t")
                    write_file.write(snippet)



if __name__ == "__main__":
    cluster_train_data(TRAIN_DATA_PATH, TEST_DATA_PATH)